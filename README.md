# Perl::Types Committee

We are the official democratically-elected organization for oversight of Perl::Types.

## Vision Statement

Our vision is a world where Perl::Types is part of the Perl core distribution and used by all major CPAN distributions.

## Mission Statement

Our mission is to develop and promote the Perl::Types high-performance data type system.
