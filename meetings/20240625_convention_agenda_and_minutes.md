# Perl::Types Committee
# Convention Agenda & Minutes

## June 25th, 2024
## 1st Annual Convention

* MEETING MINUTES DITTOED FROM SCIENCE PERL COMMITTEE CONVENTION

* Opening, 9:03pm Pacific time zone
    * 1st Annual Perl Committee (Constitutional!) Convention
    * The Perl Conference
    * Alexis Park Resort & Hotel, Las Vegas, NV

* Attendance & Introductions, Hybrid Event
    * In Person
        * Brett Estrade (high-performance computing)
        * Will Braswell (computer scientist, creator of Navi AI)
        * Venkataramana Mokkapati AKA "Ramana" (works for large semiconductor company, build CPAN-like repository of EE components, elliptical curve cryptography)
        * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
        * Dimitris Kechagias (FOO BAR)
        * Steven McDougall (FOO BAR)
        * Jan Stepanek (computational linguistics, faculty of mathematics and physics of Charles University, Prague, Czech Republic)
    * Virtual
        * Robbie Hatley (EE & computer programming in Perl)
        * Marc Perry, PhD (biologist, previous wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
        * Christos Argyropoulos, MD, PhD (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology dev for sequencing, new markers for kidney disease)
        * Mickey Macten (online security and streaming multimedia)
        * Packy Anderson (FOO BAR)
        * Thuyein Thet (RT ticketing system, wants to learn more about Perl science & AI)
        * Francisco Zaraboza (web services, wants to learn about Perl AI)

* Announcements
    * The Perl Committees Convention will be held once a year, during The Perl Conference (TPC)
        * Immediately following Science Perl Committee and followed by AI Perl Committee Conventions

* Previous Convention's Minutes
    * none

* Old Business
    * none

* New Business

    * "Forming a Permanent Society", Robert's Rules of Order Quick Study Guide, read by Will Braswell

    * Pizza available for in-person attendees, many thanks to Brett for providing

    * Bylaws using Robert's Rules of Order as basic template, read by Will Braswell
        * Motion made by Will Braswell to approve bylaws as read, seconded by Dr. Adam Russell
        * Unanimously approved

    * Charter presented by Will Braswell, President of Public Enrichment & Robotics Laboratories, to Brett Estrade, Chairman of the Science Perl Committee

    * Awards presented by Will Braswell and Brett Estrade to the following:
        * Larry A. Wall New-Unit Organizer Award AKA Perl Committee Founders' Award
            * Dr. Adam Russell (red science for SPJ paper, present, awarded on 6/25/24 at convention)
            * Jan Stepanek (red science for SPJ paper, present, awarded on 6/25/24 at convention)
            * Brett Estrade (red science for SPJ paper, present, awarded on 6/25/24 at convention)
            * George Baugh (red science for SPJ paper, not present, awarded on 6/27/24 at TPC Lightning Talks in Las Vegas, NV)
            * John Napiorkowski (blue AI for Navi project, not present, awarded on 7/3/24 at AI Perl Hackathon in Austin, TX)
            * Robert Grimes (blue AI for Navi project, not present, NEED GIVE MEDAL & CERTIFICATE AT AUSTIN PERL MONGERS)
            * Dr. Andrew O'Neil (red science for SPJ paper, not present, NEED MAIL MEDAL & CERTIFICATE)
            * Dr. Christos Argyropoulos (red science for SPJ paper, present online, certificate mailed by Brett & received on 7/5/24, NEED MAIL MEDAL)
            * Dr. Marc Perry (red science for SPJ paper reviews, present online, NEED MAIL MEDAL & CERTIFICATE)
            * Manickam Thanneermalai (red science for SPJ paper, not present, NEED MAIL MEDAL & CERTIFICATE)
            * Zaki Mughal (red science for OJS project & blue AI for Navi & TensorFlow & Cowl projects, not present, NEED MAIL MEDAL & CERTIFICATE)
            * Venkataramana Mokkapati (blue AI for TensorFlow project, present, awarded on 6/25/24 at convention)
            * Will Braswell (red science for SPJ paper & white types for Perl::Types project & blue AI for Navi & PerlGPT projects, present, awarded on 6/25/24 at convention)
        * Golden PERL Award AKA People's Choice Award
            * Voting now open to the public at perlcommunity.org
            * Voting will close in 2 days on 6/27/24 at 4:00pm Pacific Time Zone
        * Platinum PERL Award AKA Benevolent Service Award
            * Vote taken from all committee members present in-person and online
                * Tie between Brett Estrade & Will Braswell
                * Tie breaker goes to Will
            * Will Braswell (present, awarded trophy at 6/25/24 convention, NEED CERTIFICATE)
        * Diamond PERL Award AKA Editor's Choice Award of Technical Excellence
            * Introduction & announcement of recipient by SPJ Chief Reviewer Dr. Marc Perry
            * Dr. Christos Argyropoulos (present online, trophy & medal mailed by Brett & received on 7/5/24, NEED MAIL CERTIFICATE)

    * Science Perl Journal
        * Issue #1 Unboxing
        * Preprint Edition
        * $40 for normal, $80 for signed

    * Officer Elections
        * All officers were nominated by Will, and were unanimously elected
        * Chairman: Will Braswell
        * 1st Vice: Dr. Christos Argyropoulos

    * Name Tags
        * Given out to all committee members present

    * Final Remarks
        * Dr. Adam Russell
        * Dr. Christos Argyropoulos
        * Brett Estrade
        * Will Braswell

* Closing, 9:03pm
