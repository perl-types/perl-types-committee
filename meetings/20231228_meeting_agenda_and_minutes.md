# Perl::Types Committee
# Meeting Agenda & Minutes

## December 28th, 2023
## Eleventh Official Meeting

* Opening, 1:34pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Douglas Spore (Perl regular expressions, government classified system, Air Force Defense Support Program)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 12/14/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Hackathon
        * 3rd Hackathon last night
            * Zaki helped, others joined in for fun
            * We started debugging the GitLab CI, in order to eventually get all the data type tests to pass
            * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
            * We got the Inline::C and Inline::CPP tests to pass!
            * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Type::Class
            * We are ready to start moving the rest of the code into the Perl::Type namespace
        * Every 6 weeks
        * Next event 2/7/24, 7:00-9:00pm Central time zone, theme is Perl::Types Refactoring P4

    * Perl::Types PPC
        * none

* New Business

    * Douglas offers an original black Perl Mongers t-shirt, Will gratefully accepts
    * Douglas has a book signed by Tom Christianson, currently plans to keep this himself

* Closing, 2:02pm
