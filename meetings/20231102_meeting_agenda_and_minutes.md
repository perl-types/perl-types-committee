# Perl::Types Committee
# Meeting Agenda & Minutes

## November 2nd, 2023
## Seventh Official Meeting

* Opening, 1:57pm

* Attendance & Introductions (already done in preceding meeting)
    * Brett Estrade (high-speed computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)

* Announcements
    * Meet every 2 weeks on Thursday 1:30-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/19/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types
        * We did not review any software source code during this meeting

    * Perl::Types Hackathon
        * Every 6 weeks
        * Next event 11/15/23, 7-9pm Central time zone, theme is Perl::Types Refactoring P2

    * Perl::Types PPC
        * Brett Estrade will contact C.P. this week
        * C.P. can potentially help with both community & technical issues

* New Business
    * none

* Closing, 2:02pm
