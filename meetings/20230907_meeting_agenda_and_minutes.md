# Perl::Types Committee
# Meeting Agenda & Minutes

## September 7th, 2023
## Third Official Meeting

* Opening, 1:51pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL)
    * Christos Argyropoulos (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology development for sequencing, new markers for kidney disease)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Adam Russell (computer scientist, dissertation in computational geometry in Perl, leads group United Health Group subsidiary OptumAI, business AI automating building processes & reviewing documents)
    * Antanas Vaitkus (Lithuania, informatics & bioinformatics, crystallography, open database software in Perl)
    * John Kirk (no intro, microphone problems)

* Announcements
    * Meet every 2 weeks on Thursday 1:30-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings
    * 1st Vice Chairman Brett Estrade is unable to attend today

* Previous Meeting's Minutes
    * 8/24/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * Perl::Types PPC
        * Elevator Pitch
            * Will Braswell gave a report on behalf of himself and Brett Estrade
            * More replies to and from Dave Mitchell and Chris Prather on P5P mailing list
            * Chris Prather suggests 2-year process, we will see if that is viable
        * Full Proposal
            * Still working on this, will probably submit to P5P soon
            * Perl::Types is the real type system because we use the real data types from the Perl interpreter and Perl compiler
            * Other proposed or existing type systems are artificial, they can be built on top of Perl::Types

* New Business
    * none

* Closing, 2:00pm
