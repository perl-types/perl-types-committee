# Perl::Types Committee
# Meeting Agenda & Minutes

## March 6th, 2025
## Fortieth Official Meeting

* Opening, 2:07pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Mohammed Zia (uses Perl for regular expressions etc)
    * Brian Rao (Chicago, Masters in bioinformatics, works with bulk RNA, metagenomics, bacteria DNA; starting on single-cell RNA sequencing analysis; Docker, bash, cloud, Nextflow)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 2/20/25 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Old Business

    * Perl::Types Links  [ NO CHANGE 20250306 ]
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Website  [ NO CHANGE 20250306 ]
        * perlcommunity.org/types

    * Perl::Types Refactoring
        * We are currently debugging the GitLab CI, in order to eventually get all the data type tests to pass
        * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
        * We got the Inline::C and Inline::CPP tests to pass!
        * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Class
        * We moved the data types from RPerl::DataType to Perl::Type
        * We moved the data structures from RPerl::DataStructure to Perl::Structure
        * We need the files which are shared between RPerl & Perl::Types to be refactored into their own distribution some day
            * Meanwhile, we moved them to become Perl::Types for the time being, even for just the pure-Perl Perl::Types tests to pass due to dependencies
            * We renamed them all from "RPerl" to just "Perl" because they are not specific to either the RPerl compiler or the Perl::Types type system
            * We moved RPerl/Inline.pm to Perl/Inline.pm
            * We moved HelperFunctions*.* 
            * We moved RPerl/Config.pm to Perl/Config.pm 
            * We moved rperltypes* to perltypes*
            * We moved UnsignedInteger* to NonsignedInteger*
        * We fixed the following bugs:
            * Inline (or something else) is magically inserting spaces after the "unsigned" keyword; CORRELATION #rp500
                * Long-standing "unsigned _integer" bug 
                * something is scanning for the string " unsigned" because "funsigned" doesn't trigger it but "unsignedinteger" does
                * We don't know where the space is coming from, possibly Inline
                * Work-around solution is to rename the data type to "nonsigned_integer" and the package to NonsignedInteger for all user-facing components
                * We are keeping "UI" and similar names meaning "unsigned integer" for internal components only, reflecting the real underlying C code
                    * "No typemap for type unsigned _integer. Skipping unsigned _integer XS_unpack_unsigned_integer(SV *)"
                    * etc
            * Inline (or something else) is wrongly considering "return" as a C data type instead of a C operation; CORRELATION #rp501 
                * Must use return() with parentheses to avoid false error message when running `perl t/04_type_scalar.t`...
                * "No typemap for type return. Skipping return sv_newmortal()"
            * Inline (or something else) is wrongly considering parameter type declarations as typedefs?  CORRELATION #rp502 
                * We must use "(const string&)" instead of "const string&" to avoid false error messages when running `perl t/04_type_scalar.t`...
                * "No typemap for type string &. Skipping void string_substitute_global(SV *, string &, string &)"
                * Had to undo some of these #rp502 changes in String.h and String.cpp, scalar tests now passing for all 3 compile modes on local laptop!
            * Separation of type system from compiler means no more compiler pseudo-source-filter being run on type system files; CORRELATION #rp018
                * Description in comments of lib/Perl/Type/Integer.pm & friends
                    * DEV NOTE, CORRELATION #rp018: Perl::Type::*.pm files do not 'use RPerl;' and thus do not trigger the pseudo-source-filter contained in
                    * RPerl::CompileUnit::Module::Class::create_symtab_entries_and_accessors_mutators(),
                    * so *__MODE_ID() subroutines are hard-coded here instead of auto-generated there
                * "Failed test 'main::Perl__Type__Integer__MODE_ID() lives' at t/04_type_scalar.t line 120."
            * Unescaped left braces inside regexes causing many warnings for t/06_type_hash.t
                * Description in comments of t/06_type_hash.t
                    * DEV NOTE: must have extra backslash-delimited-backslash '\\' in front of backslash-delimited-left-brace '\{' to avoid warnings
                    * "Unescaped left brace in regex is passed through in regex; marked by <-- HERE in m/(?ms)^{ <-- HERE (?=.*\n ..."
                    * q{/^\\\{(?=.*\n ...
        * We got the Scalar tests to pass!
        * We got the Array & Hash tests to pass!
        * We got all the tests to pass in GitLab via Dist::Zilla!!!
        * Planning to release for 2025 summer
        * Current tasks:
            * NEED TO WORK WITH JOHN on using AI to analyze current upgrade status and try to automate remaining upgrade to avoid tedious manual labor
            * Rename all data types from underscores to double-colons, and also from backwards to forwards
                * Similar to TypeScript and Go
                * Completed the Array data structure renaming, also most of the Scalar data type renaming
                * Currently working on the Hash 2D data structure renaming, finished CPP file & starting H file
                * Focused on data types needed by PerlGPT & other AI systems
            * CPAN release once all types are renamed and all tests are passing on GitLab again
        * Next tasks:
            * Continue to remove or replace all remaining references to RPerl in Perl::Type and Perl::Structure files
            * Somehow fix seemingly-fake errors for CORRELATION #rp502, see details above
            * Continue moving the rest of the code into the Perl::Type namespace
        * Obstacles include:
            * Foo_cpp.pm files
                * Keep as part of Perl::Types for data types & structures, such as Integer_cpp.pm and Array_cpp.pm
            * RPerl grammar & parser components mixed into Foo.pm files
                * Split files apart, keep Perl/Type/Foo.pm in Perl::Types and create new RPerl/Type/Foo.pm in RPerl for compiler-specific components
                * `use Perl::Type::Foo;` inside RPerl/Type/Foo.pm file

    * Discussion & Review  [ NO CHANGE 20250306 ]
        * Looking for new C++ programmer to join the project
            * NEED POST ON NEW PROFESSIONAL PERL ASSOCIATION JOB SITE

    * Perl::Types Hackathon  [ NO CHANGE 20250306 ]
        * Every 6 weeks
        * Previous event 2/19/25, normal time 7:00-9:00pm Central time zone, theme was Perl::Types Refactoring P13
        * Next event 4/2/25, normal time 7:00-9:00pm Central time zone, theme is Perl::Types Refactoring P14

    * 3-Tier Perl Architecture  [ NO CHANGE 20250306 ]
        * Perl::Types Data Type System
        * RPerl Static/AOT Compiler
        * Perl Dynamic/JIT Compiler & VM

    * Perl Community Conference  [ NO CHANGE 20250306 ]
        * Perl::Types talk at PCC Winter 2024, will link when released
        * Perl::Types paper in SPJ issue #2, will link when published
            * NEED FINISH PAPER

    * RoboPerl Nonprofit
        * none

* New Business

    * none

* Closing, 2:15pm
