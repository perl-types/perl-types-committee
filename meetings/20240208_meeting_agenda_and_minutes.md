# Perl::Types Committee
# Meeting Agenda & Minutes

## February 8th, 2024
## Fourteenth Official Meeting

* Opening, 2:03pm

* Attendance & Introductions (already done in preceding meeting)
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Cheok Yin "CY" Fung (physics major, interested in math and quantum computing)
    * Mike Flannigan (interested in math & science in general, also astronomy & space program)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 1/25/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Links
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Refactoring
        * We are currently debugging the GitLab CI, in order to eventually get all the data type tests to pass
        * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
        * We got the Inline::C and Inline::CPP tests to pass!
        * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Class
        * We moved the data types from RPerl::DataType to Perl::Type
        * We moved the data structures from RPerl::DataStructure to Perl::Structure
        * Need to remove or replace all remaining references to RPerl in Perl::Type and Perl::Structure files
        * Then continue moving the rest of the code into the Perl::Type namespace

    * Perl::Types Hackathon
        * 4th Hackathon was last night, slow & steady wins the race
        * Every 6 weeks
        * Next event 3/20/24, SPECIAL LATER TIME 9:00-10:00pm Central time zone, theme is Perl::Types Refactoring P5

    * 3-Tier Perl Architecture
        * Perl::Types Data Type System
        * RPerl Static/AOT Compiler
        * Perl Dynamic/JIT Compiler & VM

* New Business
    * none

* Closing, 2:18pm
