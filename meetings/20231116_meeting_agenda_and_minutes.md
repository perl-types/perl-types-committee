# Perl::Types Committee
# Meeting Agenda & Minutes

## November 16th, 2023
## Eighth Official Meeting

* Opening, 1:49pm

* Attendance & Introductions (already done in preceding meeting)
    * Brett Estrade (high-speed computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/2/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types
        * We did not review any software source code during this meeting

    * Perl::Types Hackathon
        * Second Perl::Types Hackathon was held last night
            * We had a good turnout, and we were able to get the Perl::Types tests to pass for all the data types and data structures tests
            * Not yet finished on the interpret/execute tests, will continue at next Hackathon
            * May be able make our first stable release of Perl::Types after one or two more Hackathons
        * Every 6 weeks
        * Next event 12/27/23, SPECIAL HOLIDAY TIME 8:30-10:00pm Central time zone, theme is Perl::Types Refactoring P3
            * Date & time are currently subject to change due to holidays etc.

    * Perl::Types PPC
        * Brett Estrade will contact C.P. this week
        * C.P. can potentially help with both community & technical issues

* New Business
    * none

* Closing, 2:02pm
