# Perl::Types Committee
# Meeting Agenda & Minutes

## August 10th, 2023
## First Official Meeting

* Opening, 2:50pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell, Co-Founder & Acting Chairman & Acting Secretary
    * Brett Estrade, Co-Founder & Acting 1st Vice Chairman
    * Zakariyya Mughal, Co-Founder & Acting 2nd Vice Chairman
    * David Warner
    * Robbie Hatley
    * Darren Duncan
    * Nedzad Hrnjica
    * Rohit Manjrekar
    * Paul Millard
    * Duong Vu
    * Daniel Mera (no intro)
    * Tyler Bird (recap meeting only)
    * Joshua Day (recap meeting only)
    * Rajan Shah (recap meeting only)

* Announcements
    * Meet every 2 weeks on Thursday 2:30-3:00pm Central time zone
        * Immediately following 2:00pm Science Perl Committee meeting
    * Recap meeting 8/10 9:30pm tonight
    * Elections 8/24 at next meeting; Chair, 1st & 2nd Vice Chairs, Secretary / Treasurer
    * Bylaws discussion 9/7 at meeting after next

* Read previous meeting's minutes
    * none

* Old Business
    * none

* New Business

    * Perl::Types PPC
        * Elevator Pitch draft
            * https://gitlab.com/perl-types/perl-types-committee/-/snippets/2586302
            * We will submit before next meeting
        * Darren Duncan: we should consider adding more types than the Perl interpreter currently supports, such as...
            * unicode
            * character stream
            * byte stream
            * numbers w/ explicit byte lengths
            * gmp_number

    * TPF Grants Committee population
        * Need knowledgeable volunteers to join the committee

    * Monthly AI Perl Hackathon
        * Piggyback on this AI Perl Committee activity to work on Perl::Types, etc?

* Closing, 3:25pm
