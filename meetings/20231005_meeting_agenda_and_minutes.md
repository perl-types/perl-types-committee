# Perl::Types Committee
# Meeting Agenda & Minutes

## October 5th, 2023
## Fifth Official Meeting

* Opening, 1:48pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Ricardo Filipo AKA "Monsenhor Filipo" (Perl programmer currently in Orlando, also from Rio de Janeiro, Brazil; working with CalTech in LA, project from NASA for database machine, writing code for data interpreter & web services & AI & science, Algebraic Query Language AQL)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)

* Announcements
    * Meet every 2 weeks on Thursday 1:30-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 9/21/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Ricardo Filipo to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Hackathon
        * First Hackathon was very productive w/ several Perl programmers in attendance, we spent over 2 hours refactoring the Perl compiler into the Perl::Types data type system, deleted about 600 of the 2700 files, many more will need to be deleted in coming refactor work
        * Every 6 weeks
        * Next event 11/15/23, 7-9pm Central time zone, theme is Perl::Types Refactoring P2

    * Perl::Types PPC
        * Waiting for Brett Estrade to contact C.P.

* New Business

    * Perl::Types Software
        * GitLab repo updated, see Hackathon report above
        * https://gitlab.com/perl-types/perl-types

* Closing, 2:07pm
