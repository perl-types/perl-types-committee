# Perl::Types Committee
# Meeting Agenda & Minutes

## December 14th, 2023
## Tenth Official Meeting

* Opening, 1:58pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Day (industrial tooling)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/30/23 meeting minutes
    * Read by Acting Secretary Will Braswell

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Hackathon
        * Every 6 weeks
        * Next event 12/27/23, NORMAL TIME 7:00-9:00pm Central time zone, theme is Perl::Types Refactoring P3
            * Date & time are currently subject to change due to holidays etc.
            * We do still plan to have the third Perl::Types Hackathon just 2 days after Christmas
            * We are able to do the normal time of 7:00pm because the December Boy Scout meeting is canceled

    * Perl::Types PPC
        * none

* New Business
    * none

* Closing, 2:02pm
