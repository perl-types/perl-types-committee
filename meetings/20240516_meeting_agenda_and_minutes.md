# Perl::Types Committee
# Meeting Agenda & Minutes

## May 16th, 2024
## Twenty-First Official Meeting

* Opening, 2:01pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Robbie Hatley (EE & computer programming in Perl)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Josh Rabinowitz
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 5/2/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Old Business

    * Perl::Types Links  [ NO CHANGE 20240516 ]
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Website [ NO CHANGE 20240516 ]
        * perlcommunity.org/types

    * Perl::Types Refactoring  [ NO CHANGE 20240516 ]
        * We are currently debugging the GitLab CI, in order to eventually get all the data type tests to pass
        * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
        * We got the Inline::C and Inline::CPP tests to pass!
        * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Class
        * We moved the data types from RPerl::DataType to Perl::Type
        * We moved the data structures from RPerl::DataStructure to Perl::Structure
        * We need the files which are shared between RPerl & Perl::Types to be refactored into their own distribution some day
            * Meanwhile, we moved them to become Perl::Types for the time being, even for just the pure-Perl Perl::Types tests to pass due to dependencies
            * We renamed them all from "RPerl" to just "Perl" because they are not specific to either the RPerl compiler or the Perl::Types type system
            * We moved RPerl/Inline.pm to Perl/Inline.pm
            * We moved HelperFunctions*.* 
            * We moved RPerl/Config.pm to Perl/Config.pm 
            * We moved rperltypes* to perltypes*
        * Need to remove or replace all remaining references to RPerl in Perl::Type and Perl::Structure files
        * Currently editing t/04_type_scalar.t and lib/Types/Integer* to get integer tests passing
        * Run `perl t/04_type_scalar.t` and see what errors are produced, then debug
        * CPAN release once all scalar type tests are passing
        * Obstacles include:
            * Foo_cpp.pm files
            * RPerl grammar & parser components mixed into Foo.pm files
        * Then continue moving the rest of the code into the Perl::Type namespace

    * Discussion & Review
        * none

    * Perl::Types Hackathon
        * Every 6 weeks
        * Next event 6/12/24, normal time 7:00-9:00pm Central time zone, theme is Perl::Types Refactoring P7

    * 3-Tier Perl Architecture [ NO CHANGE 20240516 ]
        * Perl::Types Data Type System
        * RPerl Static/AOT Compiler
        * Perl Dynamic/JIT Compiler & VM

* New Business
    * none

* Closing, 2:07pm
