# Perl::Types Committee
# Meeting Agenda & Minutes

## March 7th, 2024
## Sixteenth Official Meeting

* Opening, 1:59pm

* Attendance & Introductions (already done in preceding meeting)
    * John Napiorkowski (maintainer of Catalyst)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Mickey Macten
    * Joshua C Oliva

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 2/8/24 meeting minutes
    * 2/22/24 meeting minutes
    * BOTH READ by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski FOR BOTH to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Links
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Refactoring
        * We are currently debugging the GitLab CI, in order to eventually get all the data type tests to pass
        * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
        * We got the Inline::C and Inline::CPP tests to pass!
        * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Class
        * We moved the data types from RPerl::DataType to Perl::Type
        * We moved the data structures from RPerl::DataStructure to Perl::Structure
        * Need to remove or replace all remaining references to RPerl in Perl::Type and Perl::Structure files
        * Then continue moving the rest of the code into the Perl::Type namespace

    * Perl::Types Hackathon
        * Every 6 weeks
        * Next event 3/20/24, SPECIAL LATER TIME 9:00-10:00pm Central time zone, theme is Perl::Types Refactoring P5

    * 3-Tier Perl Architecture
        * Perl::Types Data Type System
        * RPerl Static/AOT Compiler
        * Perl Dynamic/JIT Compiler & VM

* New Business

    * Website
        * perlcommunity.org/types
        * Now published, need to present to committee & officially release

    * Minimum Quorum Requirements
        * Will Braswell makes a motion to adopt 5 as the minimum number of committee members required for a voting quorum
        * Seconded by John Napiorkowski
        * Vote results are 100% unanimous in favor of 5 minimum committee members required for an official voting quorum from now on

* Closing, 2:05pm
