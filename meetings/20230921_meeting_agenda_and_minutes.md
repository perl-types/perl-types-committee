# Perl::Types Committee
# Meeting Agenda & Minutes

## September 21st, 2023
## Fourth Official Meeting

* Opening, 1:56pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Brett Estrade (independent Perl contractor, helping scientists in HPC, oceanography & storm surge modeling using finite element 2-D modeling)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Antanas Vaitkus (Lithuania, background in bioinformatics, working in crystallography for 10 years, open database software in Perl to collect all small molecule structures)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)
    * Robbie Hatley (EE & computer programming in Perl for 8 years)
    * Venkataramana Mokkapati AKA "Ramana" (works for large semiconductor company, build CPAN-like repository of EE components, elliptical curve cryptography)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)

* Announcements
    * Meet every 2 weeks on Thursday 1:30-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 9/7/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by FOO BAR to be accepted as read, voted unanimously

* Old Business

    * Perl::Types PPC
        * Full Proposal
            * Decided not to move too quickly, currently working on this slowly w/ Brett
            * Brett will contact C.P. and ask him to help guide us

* New Business

    * Perl::Types Hackathon
        * Create regular event
            * Repeat every 6 weeks
            * First event 10/4/23, 7-9pm Central time zone, theme is PPC Proposal and/or Perl::Types Programming
            * Moved by Brett Estrade, second by Antanas Vaitkus, voted unanimously

* Closing, 1:59pm
