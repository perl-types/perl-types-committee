# Perl::Types Committee
# Meeting Agenda & Minutes

## November 30th, 2023
## Ninth Official Meeting

* Opening, 2:37pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/16/23 meeting minutes
    * NOT read by Acting Secretary Will Braswell, tabled

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Hackathon
        * We made good progress at the second Perl::Types Hackathon
        * Every 6 weeks
        * Next event 12/27/23, SPECIAL HOLIDAY TIME 8:30-10:00pm Central time zone, theme is Perl::Types Refactoring P3
            * Date & time are currently subject to change due to holidays etc.

    * Perl::Types PPC
        * Brett Estrade will contact C.P.
        * C.P. can potentially help with both community & technical issues

* New Business
    * none

* Closing, 2:38pm
