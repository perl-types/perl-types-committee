# Perl::Types Committee
# Meeting Agenda & Minutes

## October 19th, 2023
## Sixth Official Meeting

* Opening, 1:47pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Brett Estrade (high-speed computing)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Minas Polychronidis (in Greece, owns think.gr software company)

* Announcements
    * Meet every 2 weeks on Thursday 1:30-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/5/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Software
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types
        * We reviewed the in-development GitLab repo and the placeholder CPAN distribution

    * Perl::Types Hackathon
        * Every 6 weeks
        * Next event 11/15/23, 7-9pm Central time zone, theme is Perl::Types Refactoring P2

    * Perl::Types PPC
        * Brett Estrade will watch video of previous Perl::Types Hackthon, then will contact C.P.
        * C.P. can potentially help with both community & technical issues

* New Business
    * none

* Closing, 2:07pm
