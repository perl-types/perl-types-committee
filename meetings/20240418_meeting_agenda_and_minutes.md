# Perl::Types Committee
# Meeting Agenda & Minutes

## April 18th, 2024
## Nineteenth Official Meeting

* Opening, 2:08pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:00pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings

* Previous Meeting's Minutes
    * 4/4/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Adam Russell to be accepted as read, voted unanimously

* Old Business

    * Perl::Types Links [ NO CHANGE 20240418 ]
        * https://gitlab.com/perl-types/perl-types
        * https://metacpan.org/dist/Perl-Types

    * Perl::Types Refactoring
        * We are currently debugging the GitLab CI, in order to eventually get all the data type tests to pass
        * We got a list of all the dependencies found by `dzil listdeps --missing`, manually disabled them all, and are manually re-enabling them one at a time in order to determine which are required by the data type system and which are caused by leftover compiler code
        * We got the Inline::C and Inline::CPP tests to pass!
        * We moved the base class from RPerl::CompileUnit::Module::Class to Perl::Class
        * We moved the data types from RPerl::DataType to Perl::Type
        * We moved the data structures from RPerl::DataStructure to Perl::Structure
        * Need to remove or replace all remaining references to RPerl in Perl::Type and Perl::Structure files
        * Currently editing t/04_type_scalar.t and lib/Types/Integer* to get integer tests passing
        * Run `perl t/04_type_scalar.t` and see what errors are produced, then debug
        * CPAN release once all scalar type tests are passing
        * Obstacles include:
            * Foo_cpp.pm files
            * RPerl grammar & parser components mixed into Foo.pm files
            * Do we need RPerl/Inline.pm & HelperFunctions*.* & Config.pm (and possibly other RPerl files) for the pure-Perl Perl::Types tests to pass?
                * If so, move those RPerl files to become part of Perl::Types instead
        * Then continue moving the rest of the code into the Perl::Type namespace

    * Perl::Types Hackathon [ NO CHANGE 20240418 ]
        * Every 6 weeks
        * Next event 5/1/24, normal time 7:00-9:00pm Central time zone, theme is Perl::Types Refactoring P6

    * 3-Tier Perl Architecture [ NO CHANGE 20240418 ]
        * Perl::Types Data Type System
        * RPerl Static/AOT Compiler
        * Perl Dynamic/JIT Compiler & VM

    * Website [ NO CHANGE 20240418 ]
        * perlcommunity.org/types

* New Business
    * none

* Closing, 2:16pm
