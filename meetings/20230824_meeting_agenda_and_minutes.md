# Perl::Types Committee
# Meeting Agenda & Minutes

## August 24th, 2023
## Second Official Meeting

* Opening, 2:45pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (creator of Navi AI)
    * Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * David Warner (AI sales & marketing)
    * Brett Estrade (high-speed computing)
    * Antanas Vaitkus (University in Lithuania, crystallography, wants to learn AI)
    * Somanath Wagh (Mumbai, India; wants to learn AI)
    * Leo Grapendaal (Dutchman living in Germany, interested in AI generating text for publications)
    * Glenn Holmgren (no introduction, audio problems)
    * Manickam Thanneermalai (EE design engineer, designing chips, Perl automates work in semiconductor industry, Synopsis & Cadence tool vendors have AI algorithms, building ARM-based system-on-chip for automotive AI)

* Announcements
    * Meet every 2 weeks on Thursday 1:45-2:15pm Central time zone
        * Immediately following Science Perl Committee and followed by AI Perl Committee meetings
    * Bylaws discussion starting 9/7/23 at next meeting

* Previous Meeting's Minutes
    * none

* Old Business

    * Perl::Types PPC
        * Elevator Pitch
            * Submitted by Brett to P5P
            * Brett talked about replies to Dave Mitchell, others who are genuinely supportive such as Chris Prather etc, various confusion & conflation
        * Full Proposal
            * Currently working on this, will submit to P5P soon
            * Brett knew it was going to be difficult, don't be shy about having your name on the Perl::Types Committee, it is your badge of honor

* New Business

    * TPF report
        * Will Braswell presented Perl::Types Committee overview to TPF Community Reps Committee 8/18/23

    * Officer Elections
        * Chairman, Will Braswell nominated by Brett Estrade, no other nominations, elected unanimously
        * 1st Vice Chairman, Brett Estrade nominated by Will Braswell, no other nominations, elected unanimously

* Closing, 2:59pm
