# Perl::Types Committee
# Membership Roster

## Officers

* Will Braswell, Chairman, elected 8/24/23, re-elected 6/25/24
* Dr. Christos Argyropoulos, 1st Vice Chairman, elected 6/25/24


## Current

* Scott Adams
* Achmad Yusri Afandi
* Packy Anderson
* Nikolay Arbatov
* Christos Argyropoulos, MD, PhD
* Jeb Bacon
* Kai Baker
* Jay Battikha
* George Baugh
* Krasimir Berov (AKA Красимир Беров)
* Tyler Bird
* Wil Blake
* Will Braswell, Co-Founder
* Anne Brown
* Julian Brown
* Guido Brugnara
* Nick Busigin
* Ryan Cabaneles
* Alex Casamassima
* Egon Choroba
* Mitchko Christophe
* Smart Code
* Bar Bojan Coders
* Joshua Day
* Deepa
* Roger Doss
* Brett Estrade, Co-Founder
* Ricardo Filipo
* Cheok Yin "CY" Fung
* Kieren Goldfish
* Alex Gomez
* Leo Grapendaal
* Henrique Gusmao
* Sangyong Gwak
* Zahirul Haq
* Dan Harris
* Robbie Hatley
* Glenn Holmgren
* Nedzad Hrnjica
* Keith Hsieh
* Tracey Jacoby
* Anuj Jain
* Witold Janczar
* Bartosz Jarzyna
* Sumedha Jeewan
* Eric Jeskey
* Lewis Johanne
* Lee Johnson
* John D Jones III
* Dimitris Kechagias
* Justin Kelly
* Vlado Keselj
* Krzysztof Kielak
* Hansom Kim
* John Kirk
* Venkatesh Kumar
* Dirk Linder
* James Linus
* Abraham Llave (AKA إبراهيم مفتاح)
* Wang Lun
* Marco Aurelio Macae
* Mickey Macten
* Reinier Maliepaard (AKA Barbe Vivien)
* Rohit Manjrekar
* John Martinez
* Daniel Mera
* Paul Millard
* Venkataramana Mokkapati
* Ali Moradi
* Daryl Morning
* Zakariyya Mughal, Co-Founder
* Fernando Munguia
* Hisham Musa
* John Napiorkowski, Co-Founder
* Connie New
* Al Newkirk
* Joshua C Oliva
* Drew O'Neil
* Gerard Onerom
* John P
* Sarswati Kumar Pandey
* Sibananda Pani
* Vaibhav Patil (AKA वैभव पाटील)
* James Pattie
* Jakub Pawlowski
* Rui Pereira
* Jason Philbin
* Minas Polychronidis
* Josh Rabinowitz
* Yulian Radev
* Brian Marquez Inca Roca
* Steve Rogerson
* Laurent Rosenfeld
* Stephane Roux
* Adam Russell, PhD
* Robert Ryley
* Siddharth Satyapriya
* Rajan Shah
* Prasanna Silva
* Saman Senju Simorangkir
* Douglas Spore
* Benjamin Paul Suntrup
* Manickam Thanneermalai
* Thuyein Thet
* Jovan Trujillo
* Antony Use
* Antanas Vaitkus
* Duong Vu
* Somanath Wagh
* Bo Cheng Wei
* Jeff Yoak
* Ruey-Cherng Yu
* Francisco Zarabozo
* Emanuele Zeppieri
* Mohammed Zia


## Invited

* Haider Ali
* Camila Chavarro
* Rick Croote
* Andrass Ziska Davidsen
* Jacob Dev
* Boyd Duffee
* Erki Ferenc
* Krzysztof Flis
* Sven Gelbhaar
* Ankita Ghadge-More
* Robert Grimes
* Stefan Hornburg
* Roy Hubbard
* Kusa Manohar
* Anatol Mazur
* Rajesh Kumar Mallah
* Andrew Mehta
* Daniel Mita
* Su Mu
* Saiful Islam Musafir
* Dharmendra Namdeo
* Emil Perhinschi
* Vugar Bakhshaliyev (AKA Rituda Perl)
* Matthew Price
* Mohd Rashid
* Hila Sapirstein
* Mike Schienle
* Daniel Sherer
* Ludovic Tolhurst-Cleaver
* Brandon Wood


## Need Invite

* Avery Adams (AKA oldtechaa, AKA Beadle)
* Rob De la Cruz
* Prashant Deore

## Declined

* none
